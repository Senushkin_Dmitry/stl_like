//
//  main.cpp
//  CTreeBuilder
//
//  Created by Dmitry on 05.02.16.
//  Copyright © 2016 Dmitry. All rights reserved.
//

#include <iostream>
#include "CTree.hpp"
typedef CTree<int> tree_of_int;
typedef CTree<int>* ptr_tree_of_int;
typedef Node<int> node_of_int;
typedef Node<int>* ptr_node_of_int;


int main(int argc, const char * argv[]) {
    ptr_tree_of_int tree = new CTree<int>;
    tree->initialize(0);
    ptr_node_of_int son1 = new Node<int>;
    son1->core = 1;
    tree->insert_child(son1, tree->get_head());
    ptr_node_of_int node2 = new Node<int>;
    node2->core = 2;
    tree->insert_brother(node2, son1);
    ptr_node_of_int node3 = new Node<int>;
    node3->core = 3;
    tree->insert_brother(node3, son1);
    ptr_node_of_int brother1 = new Node<int>;
    ptr_node_of_int brother2 = new Node<int>;
    ptr_node_of_int brother3 = new Node<int>;
    ptr_node_of_int brother4 = new Node<int>;
    brother1->core = 21;
        brother2->core = 22;
        brother3->core = 23;
        brother4->core = 24;
    tree->insert_child(brother1, node2);
    tree->insert_brother(brother2, brother1);
    tree->insert_brother(brother3, brother1);
    tree->insert_brother(brother4, brother1);
    
    ptr_node_of_int son11 = new Node<int>;
    ptr_node_of_int son111 = new Node<int>;
    ptr_node_of_int son1111 = new Node<int>;
    son11->core = 11;
    son111->core = 111;
    son1111->core = 1111;
    tree->insert_child(son11, son1);
    tree->insert_child(son111, son11);
    tree->insert_child(son1111, son111);
    //tree->_delete_child(node2);
    
    CTree<int>::iterator it = tree->begin();
    std::cout << *it << "\n";
    //tree->show();
    for (CTree<int>::iterator it = tree->begin(); it != tree->end(); ++it) {
        std::cout << *it << " iterator\n";
    }
    return 0;
}
