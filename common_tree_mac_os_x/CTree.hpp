//
//  CTree.hpp
//  CTreeBuilder
//
//  Created by Dmitry on 07.02.16.
//  Copyright © 2016 Dmitry. All rights reserved.
//

#ifndef CTree_hpp
#define CTree_hpp

#include <stdio.h>
#include <iostream>


class iterator;                                                                                                         // итератор, по нему будет проходить обход дерева
template<class D>
struct Node{
    D core;                                                                                                             // инф. поле
    Node<D> *parent, *child, *next, *previous;                                                                             // указатель на потомка и брата
    Node():parent(0), child(0), next(0), previous(0){};                                                                 // конструктор по умолчанию
    Node(const Node<D> *node):parent(node->parent), child(node->child), next(node->next), previous(node->previous){};      // конструктор копирования
    Node(const D core):parent(0), child(0), next(0), previous(0), core(core){};                                         // конструктор с параметрами
};

template<class T>
class CTree{
    
    typedef Node<T> instance_node;              // объект шаблонной структуры типа Т
    typedef Node<T>* pointer_node;              // указатель на такой объект
    
private:
    pointer_node head;                                             // главный элемент, самый верхний уровень
    pointer_node pre_head;                                         // требуется для того чтобы корректно обходить дерево, включая end()
public:
    friend class iterator;
    
    CTree():head(0), pre_head(0){};;                        // конструктор по умолчанию
    CTree(const int core);                                  // конструктор с параметрами
    ~CTree(){                                               // деструктор
        if (!is_empty()) {
            _delete_child(head);
            delete head;
            delete pre_head;
        }
    }
    
    class iterator{
    protected:
        pointer_node ptr;                           // бегунок
        bool flag_bottom = true;                    // флажок необходимый для обхода дерева(запрещает падение на уровень ниже)
    public:
        iterator(pointer_node ptr):ptr(ptr){};      // конструктор конструктор с параметрами
        iterator():ptr(0){};                        // конструктор по умолчанию
        
        typedef iterator& reference_iterator;               // ссылка на тератор
        typedef const iterator& const_reference_iterator;   // константная ссылка на итратор
        
        reference_iterator operator++(){                // перегрузка оператора пре инткремента
            if (ptr->child && flag_bottom) {
                ptr = ptr->child;
                return *this;
            }else if (ptr->next){
                ptr = ptr->next;
                while (ptr->child){
                    ptr = ptr->child;
                }
                flag_bottom = true;
                return *this;
            }else if (ptr->parent){
                ptr = ptr->parent;
                flag_bottom = false;
                return *this;
            }
            return *this;
        }
        reference_iterator operator=(const_reference_iterator it){      // перегрузка оператора присваиваня
            ptr = it.ptr;
            return *this;
        }
        T& operator*(){                 // оператор взятия данных
            return ptr->core;
        }
        pointer_node operator->(){      // оператор взятия указателя
            return ptr;
        }
        bool operator ==(const_reference_iterator it){  // перегрузка оператора равенства
            return ptr == it.ptr;
        }
        bool operator !=(const_reference_iterator it){  // перегрузка оператора неравенства
            return ptr != it.ptr;
        }
    };
    
    pointer_node get_head() { return head; }    // возвращает указатель на голову дерева
    bool is_empty(){                // проверка на пустоту дерева
        if (head == NULL) {
            return true;
        }else{
            return false;
        }
    }
    void initialize(const T core){  // инициализация дерева
        head = new Node<T>;
        head->core = core;
        head->next = NULL;
        head->previous = NULL;
        head->child = NULL;
        pre_head = new Node<T>;
        pre_head->core = 1000;
        pre_head->next = NULL;
        pre_head->previous = NULL;
        pre_head->child = head;
        head->parent = pre_head;
    }
    void _show(const pointer_node markNode){        // рекурентная функция для отображения дерева на экране
        pointer_node tmpSon = markNode->child;
        std::cout << markNode->core << "\n";
        while (tmpSon) {
            _show(tmpSon);
            tmpSon = tmpSon->next;
        }
        
    }
    void show(){                        // полный вывод дерева на экран
        if (!is_empty()) {
            if (!head->child) {
                std::cout << "Ячейка не имеет потомков\n";
            }else{
                _show(head);
            }
        }
    }
    void _delete_child(pointer_node markNode){      // рекурентное удаление всех потомков
        pointer_node tmp = markNode->child;
        markNode->child = NULL;
        while (tmp) {
            _delete_child(tmp);
            pointer_node tmp1 = tmp->next;
            delete tmp;
            tmp = tmp1;
        }
    }
    void insert_brother(pointer_node insertNode, pointer_node markNode){    // добваление брата указанной ячейке
        insertNode->next = NULL;
        if(markNode->next == NULL){
            markNode->next = insertNode;
            insertNode->previous = markNode;
            insertNode->parent = markNode->parent;
        }else{
            pointer_node tmp = markNode->next;
            while (tmp->next != NULL) {
                tmp = tmp->next;
            }
            tmp->next = insertNode;
            insertNode->previous = tmp;
            insertNode->parent = markNode->parent;
        }
    }
    void insert_child(pointer_node insertNode, pointer_node markNode){      // добавление потомка указанной ячейке
        insertNode->next = NULL;
        if (markNode->child == NULL) {
            markNode->child = insertNode;
            insertNode->parent = markNode;
        }else{
            pointer_node tmp = markNode->child;
            while (tmp->next) {
                tmp = tmp->next;
            }
            tmp->next = insertNode;
            insertNode->previous = tmp;
            insertNode->parent = markNode;
        }
    }
    pointer_node find_begin(){                  // поиск самой нижней левой ячейки
        if (!is_empty()) {
            pointer_node end = head;
            while (end->child) {
                end = end->child;
            }
            return end;
        }else{
            std::cout << "Структура данных: деверо, пуста\n";
            return NULL;
        }
    }
    iterator begin(){                       // возвращает итератор начала дерева
        return iterator(find_begin());
    }
    iterator end(){                         // возвращает итератор конца дерева
        return iterator(pre_head);
    }
};

#endif /* CTree_hpp */
