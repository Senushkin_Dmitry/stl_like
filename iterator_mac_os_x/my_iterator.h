//
//  my_iterator.h
//  iterator
//
//  Created by Dmitry on 27.02.16.
//  Copyright © 2016 Dmitry. All rights reserved.
//

#ifndef my_iterator_h
#define my_iterator_h

#include <iostream>

#define END_OF_LIST 0x001

///////////////////////////////////////////
// Описание категорий итераторов //////////
///////////////////////////////////////////

template<class Type> class list;
template<class Type> class forward_iterator;

namespace iterator_tag
{
	struct input_iterator_tag {};
	struct output_iterator_tag {};
	struct forward_iterator_tag {};
	struct bidirectional_iterator_tag {};
	struct random_access_iterator_tag {};
}


///////////////////////////////////////////
// Опиание ячейка контейнера //////////////
///////////////////////////////////////////


template<class D>
struct list_node{
	D data;
    list_node<D>* next;
    list_node<D>():data(0), next(0) {};
    list_node<D>(D data):data(data), next(0){};
};



///////////////////////////////////////////
// Базовый класс итератора ////////////////
///////////////////////////////////////////

template<class Type, class C, class Pt = Type*, class Ref = Type&, class Dist = ptrdiff_t>
class iterator_base
{
public:

	typedef list_node<Type> node;
	typedef iterator_base<Type, Type*, Type&> iterator;
	typedef iterator_base<Type, const Type*, const Type&> const_iterator;

	typedef C iterator_category;
    typedef Type value_type;
    typedef Dist difference_type;
    typedef Pt poiner;
    typedef Ref reference;
    typedef size_t size_type;

public:

	iterator_base():m_node(0){};
    iterator_base(node *ptr){ m_node = ptr;};
	iterator_base(iterator& it):m_node(it.m_node){};
	~iterator_base(){};

protected:

	list_node<Type> * m_node;

};

////////////////////////////////////////////////
// Определение наследника - forward_iterator ///
////////////////////////////////////////////////

template<class Type>
class forward_iterator: public iterator_base<Type, iterator_tag::forward_iterator_tag>
{
public:

	typedef list_node<Type> node;
	typedef forward_iterator<Type> iterator;

    typedef iterator_tag::forward_iterator_tag iterator_category;
    typedef Type* pointer;
    typedef Type& reference;
    typedef ptrdiff_t difference_type;
    typedef Type value_type;
    typedef size_t size_type;

public:

	forward_iterator():iterator_base<Type, iterator_tag::forward_iterator_tag>(){};
	forward_iterator(list_node<Type>* ptr):iterator_base<Type, iterator_tag::forward_iterator_tag>(ptr){};
    forward_iterator(iterator_base<Type, iterator_tag::forward_iterator_tag> it):iterator_base<Type, iterator_tag::forward_iterator_tag>(it){};
    forward_iterator(forward_iterator const &it):iterator_base<Type, iterator_tag::forward_iterator_tag>(it.m_node){};

public:

	iterator& operator++()
	{
		if (this->m_node->next)
		{
			this->m_node = this->m_node->next;
		}

		return *this;
	}
	iterator operator++(int x)
	{
		iterator it(*this);
		++(*this);
		return it;
	}
	reference operator*()
	{
		return this->m_node->data;
	}
	bool operator==(const iterator it)
	{
		return this->m_node == it.m_node;
	}
	bool operator!=(const iterator it)
	{
		return !(this->m_node == it.m_node);
	}
	iterator& operator=(const iterator& it)
	{
		if (this == &it) return *this;

		this->m_node = it.m_node;
		return *this;
	}
};


template<class Type>
class bidirectional_iterator: public forward_iterator<Type>
{
public:

    typedef list_node<Type> node;
    typedef bidirectional_iterator<Type> iterator;

    typedef iterator_tag::forward_iterator_tag iterator_category;
    typedef Type* pointer;
    typedef Type& reference;
    typedef ptrdiff_t difference_type;
    typedef Type value_type;
    typedef size_t size_type;

public:

    bidirectional_iterator():forward_iterator<Type>(0), m_list(0){};
    bidirectional_iterator(list_node<Type>* ptr, list<Type>* ptr2):forward_iterator<Type>(ptr), m_list(ptr2){};
    bidirectional_iterator(iterator_base<Type, iterator_tag::bidirectional_iterator_tag> it):iterator_base<Type, iterator_tag::bidirectional_iterator_tag>(it){};
    bidirectional_iterator(bidirectional_iterator const &it): forward_iterator<Type>(it.m_node), m_list(it.m_list){};

public:

    iterator& operator--()
    {
        list_node<Type>* temp = this->m_list->getHead();
        while (temp->next != this->m_node) 
        {
            temp = temp->next;
        }
        this->m_node = temp;
        return *this;
    }

    iterator operator--(int x){
        bidirectional_iterator it(*this);
        --(*this);
        return it;
    }

protected:

    list<Type> * m_list;
};


///////////////////////////////////////////
// Описание контейнера ////////////////////
///////////////////////////////////////////


template<class T>
class list
{
public:
    
    list():m_head(0), m_end_list(0){};
    ~list()
    {
        while(m_head)
        {
            list_node<T> * temp = m_head->next;
            delete m_head;
            m_head = temp;
        }
    }
    
public:
    
    list_node<T> * getHead() { return m_head; }
    list_node<T> * getEndList() { return m_end_list; }
    bool isEmpty(){ return !(bool)m_head; }
    
    
    void push(T data)
    {
        if (m_head)
        {
            list_node<T>* temp = m_head;
            list_node<T>* temp2 = new list_node<T>;
            temp2->data = data;
            temp2->next = m_end_list;
            while (temp->next  && temp->next != m_end_list)
            {
                temp = temp->next;
            }
            temp->next = temp2;
        }
        else
        {
            m_head = new list_node<T>;
            m_end_list = new list_node<T>;
            m_head->data = data;
            m_head->next = m_end_list;
            m_end_list->data = END_OF_LIST;
        }
    }
    
    
    void show()
    {
        list_node<T> * temp = m_head;
        if (!isEmpty())
        {
            std::cout << "Contents list: \n\n";
            while (temp && temp != m_end_list)
            {
                std::cout << temp->data << std::endl;
                temp = temp->next;
            }
        }
        else
        {
            std::cout << "List is empty!";
        }
    }
    
public:
    
    forward_iterator<T> fbegin(){ return forward_iterator<T>(m_head, this); }
    forward_iterator<T> fend(){ return forward_iterator<T>(m_end_list, this); }
    bidirectional_iterator<T> bbegin(){ return bidirectional_iterator<T>(m_head, this); }
    bidirectional_iterator<T> bend(){ return bidirectional_iterator<T>(m_end_list, this); }
    
    
protected:
    
    list_node<T> * m_head;
    list_node<T> * m_end_list;
    
};



#endif /* my_iterator_h */
