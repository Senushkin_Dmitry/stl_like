//
//  main.cpp
//  iterator
//
//  Created by Dmitry on 27.02.16.
//  Copyright © 2016 Dmitry. All rights reserved.
//

//#include <iostream>
#include "my_iterator.h"
#include <algorithm>

int main(int argc, const char * argv[]) {
    

    list<int> * test = new list<int>;
    for (auto i = 0; i < 5; i++) {
        test->push(i);
    }
    test->show();
    std::cout << "\n";
    for(bidirectional_iterator<int> it = --test->bend(); it != test->bbegin(); it--)
    {
        std::cout << *it << "\n";
    }
    bidirectional_iterator<int> p = std::find(test->bbegin(), test->bend(), 1);
    std::cout << "\n\n" << *p << "\n";
    return 0;
}
