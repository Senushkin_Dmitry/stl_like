//
//  main.cpp
//  allocator
//
//  Created by Dmitry on 26.03.16.
//  Copyright © 2016 Dmitry Senushkin. All rights reserved.
//
#include "allocator.h"

#include <iostream>
#include <vector>
#include <string>

int main(int argc, const char * argv[]) {
    
    std::vector<std::string, allocator_pool<std::string> >* vec = new std::vector<std::string, allocator_pool<std::string> >;
    
    //vec->reserve(1000);
    for(int i = 0; i < 1000; i++)
    {
        vec->push_back(std::to_string(i));//at(i) = i;
        std::cout << vec->at(i) << "\n";
        std::cout << "cap = " <<vec->capacity() << "\n";
    }
    //vec->reserve(2000);
    //std::cout << vec->capacity() << "\n";
    //delete vec;
    //std::cout << vec->size() << "\n";
    
    //vec->
    //std::cout << vec->capacity() << "\n";
    //vec->push_back(std::to_string(100));
    //std::cout << vec->at(62) << "\n";
    //vec.push_back(10);
    //vec.reserve(200);
    /*for(int i = 0; i < 20000; i++)
    {
        //vec.push_back(i);
        std::cout << vec->at(i) << "\n";
    }*/
    
    return 0;
}
