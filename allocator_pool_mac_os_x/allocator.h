//
//  allocator.h
//  allocator
//
//  Created by Dmitry on 26.03.16.
//  Copyright © 2016 Dmitry Senushkin. All rights reserved.
//

#include <iostream>
#include "malloc.h"
#ifndef allocator_h
#define allocator_h


template<class Type, class Size>
struct buffer_pool {
    
    
    uint8_t *mem;       /*  указатель на буфер байтов  */
    bool *chunks;       /*  доступность ячейки (1 да 0 нет)  */
    Size totalSize; /*  общий размер (объекты)  */
    
    buffer_pool(){};
    
    void* allocate_if_mem_is_exist(Size size)       /* size - количество элементов */
    {
        if(size > totalSize)
            return NULL;
        
        void* ptr;
        
        for(auto i = 0; i < totalSize; i++)
        {
            if(!chunks[i])
            {
                continue;
            }
            int cnt = 0;
            int begin = i;
            ptr = mem + i * sizeof(Type);
            
            while(chunks[i])
            {
                cnt++;
                i++;
            }
            
            if(cnt >= size)
            {
                for(auto i = begin; i < begin+size/sizeof(Type); i++)
                {
                    chunks[i] = false;
                }
                return ptr;
            }
        }
        
        return NULL;
    }
    
    void deallocate_from_pointer(Type* p)
    {
        unsigned long dif = (p - static_cast<Type*>((void *)mem)) / sizeof(Type);
        chunks[dif] = 1;
    }
    
};


template <class T, class Pt = T*, class CPT = const T*, class Ref = T&, class CRef = const T&, class Sz = size_t, class Dist = ptrdiff_t>
class allocator_pool
{
    
protected:
    
    buffer_pool<T, Sz> * buf;
    
public:

	typedef T                 	value_type;
  	typedef Pt       			pointer;
  	typedef CPT 				const_pointer;
  	typedef Ref       			reference;
  	typedef CRef 				const_reference;
  	typedef Sz      			size_type;
  	typedef Dist                difference_type;

public:

  	pointer address(reference x) const { return &x; }
  	const_pointer address(const_reference x) const { return &x; }
    size_type max_size() const { return static_cast<size_type>(buf->totalSize);}

  	pointer allocate(size_type n, const_pointer = 0)
  	{
        void * p = buf->allocate_if_mem_is_exist(n);
  		if(!p)
            throw std::bad_alloc();
  		return static_cast<pointer>(p);
  	}
  	void deallocate(pointer p, size_type)           // не возвращяет память буфера системе до завершения процесса
  	{
        buf->deallocate_from_pointer(p);
  	}
  	void construct(pointer p, const_reference x)
  	{
  		new(p) value_type(x);
  	}
  	void destroy(pointer p) 
  	{
  		p->~value_type();
  	}

public:

    allocator_pool()
    {
        buf = new buffer_pool<T, Sz>();
        buf->totalSize = 1000;
        //buf->offset = 0;
        buf->mem = new uint8_t[buf->totalSize*sizeof(value_type)];
        buf->chunks = new bool[buf->totalSize];
        
        for(auto i = 0; i < buf->totalSize; i++)
        {
            buf->chunks[i] = true;
        }
    };
    allocator_pool(const allocator_pool& alloc){};
	~allocator_pool()
    {
        delete buf;
    };

	template<class U>
	allocator_pool(const allocator_pool<U>& ) {};

	template<class U>
	struct rebind { typedef allocator_pool<U> other; };

public:

	void operator=(const allocator_pool& ) {};

};


#endif /* allocator_h */
